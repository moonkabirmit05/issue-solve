//selet field data get
$("#Committee_member_id :selected").text();

// Row Delete of grid page code for php 7
function deleteRows(element) {
    var message = "Are you sure you want to Delete ?";
    alertify.confirm(message, function (e) {
        if (e) {
            var idCounter = $(this).attr("id");
            $(element).parents("tr.cartList").remove();
            $("#tbl td.sno").each(function (index, element) {
                $(element).text(index + 1);
            });
            newArr[idCounter] = 0;
            alertify.success('Row Deleted From Grid');
        } 
    });
}

"<input onclick=\"deleteRows($(this))\" title=\"remove\" id='" + sl + "' type=\"button\" class=\"rdelete dltBtn\"/>" +

// each row get value same class
var overall_discount = Number($("#SellItems_overallDiscount").val());
if(overall_discount > 0){
    var inputs = $(".cartList");
    for(var i = 0; i < inputs.length; i++){
        console.log($("#line_total"+i).val());
    }
}

// get value for same siblings
$(".line_total").each(function (index, element) {    
    console.log($(element).siblings('.modelId').val()+"model id");
});

// ajax call
$.ajax({
    url: '<?php echo Yii::app()->createAbsoluteUrl('/accounting/chartOfAc/GetHeadListByRootId'); ?>',
    cache: false,
    type: "POST",
    data: {
        loanType: loanType,
    },
    success: function(data) {
        $("#CiPayment_payment_bank_id").html(data);
    }
});

// keypress trigger from outside
$("#model_id_text").focus().trigger( { type: 'keydown', which: 32 });


// Sum of values from different divs with the same class
var sum = 0;
$('.totalprice').each(function(){
    sum += parseFloat(this.value);
});

// js nan check
var qty = parseInt($("#PartyBookingDetails_qty").val());
if (isNaN(qty) || qty == '' || qty < 0)
    qty = 0;

// text field number check
$('.onlyNumber').keyup(function(){
    var val = $(this).val();
    if(isNaN(val)){
        val = val.replace(/[^0-9\.]/g,'');
        if(val.split('.').length>2)
            val =val.replace(/\.+$/,"");
    }
    $(this).val(val);
});
// hit close icon reload location
$(function () {
    $(".ui-icon-closethick").click(function () {
        window.location.href = "<?php echo Yii::app()->baseUrl; ?>/receivedPurchase/adminReceive";
    });
});


// table column merge js
$(document).ready(function () {
    var topMatchTd;
    var previousValue = "";
    var mergeNested = "";
    var rowSpan = 1;
    for (var i = 0; i <= 7; i++) {
        $('.merge-column' + i).each(function () {
            if ($(this).text() == previousValue && $(this).attr('so_no') == mergeNested) {
                rowSpan++;
                $(topMatchTd).attr('rowspan', rowSpan);
                $(this).remove();
            } else {
                topMatchTd = $(this);
                rowSpan = 1;
            }
            previousValue = $(this).text();
            mergeNested = $(this).attr('so_no');
        });
    }
});