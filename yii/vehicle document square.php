public function actionVehicleDocumentInfoUpload() {
        $fileExist = file_exists(Yii::app()->basePath . '/../upload/vd.xls');
        if ($fileExist) {
            Yii::import('ext.phpexcel.PHPExcel', true);
            $objReader = new PHPExcel_Reader_Excel5;
            $objPHPExcel = $objReader->load('upload/vd.xls');
            $objWorksheet = $objPHPExcel->getActiveSheet();
            $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
            $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
            $zero_val = 0;
            for ($row = 2; $row <= $highestRow; ++$row) {
                $reg_no = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
                $renewal_by_code = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                $issue_date_data = $objWorksheet->getCellByColumnAndRow(2, $row)->getValue();
                $durationData = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
                $fitnessRenew = array("1" => "Six Months", "2" => "One Year", "3" => "Two Years", "4" => "Three Years", "5" => "Four Years", "6" => "Five Years");


                $vehicle_data = VehicleInfo::model()->findByAttributes(array('reg_no'=>$reg_no));
                $vehicle_id = $vehicle_data !="" ? $vehicle_data->id : NULL;
                $renewal_data = Employees::model()->findByAttributes(array('emp_id_no'=>$renewal_by_code));
                $renewal_by = $renewal_data !="" ? $renewal_data->id : NULL;
                $issue_date = date("Y-m-d", strtotime(date($issue_date_data)));
                $duration = array_search($durationData,$fitnessRenew);
                $expense_with_vat = trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
                $other_amount = trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());


                // echo $vehicle_id.' ---- '.$renewal_by.' ---- '.$issue_date.' -------- '.$duration.' ------------ '.$expense_with_vat.' --------- '.$other_amount."<br>";
                // exit;


    
                $current_unit_type = $current_unit_id = $unit_name_id = $vtype_id = '';
                $vehicleInfo = VehicleInfo::model()->findByPk($vehicle_id);
                if ($vehicleInfo) {
                    $current_unit_type = $vehicleInfo->current_unit_type;
                    $current_unit_id = $vehicleInfo->current_unit_id;
                    $unit_name_id = $vehicleInfo->unit_name_id;
                    $vtype_id = $vehicleInfo->vtype_id;
                }
    
                if($vtype_id == 10){
                    $model = new VehicleDocumentInfo;
                    $max_sl_no = VoucherNoFormate::model()->maxValOfThis('VehicleDocumentInfo', 'max_sl_no', 'maxSlNo');
                    $todayDate = date('Y', strtotime(date('Y-m-d')));
                    $slNo = str_pad($max_sl_no, 5, "0", STR_PAD_LEFT) . "/" . $todayDate;
                    $model->max_sl_no = $max_sl_no;
                    $model->sl_no = $slNo;
                    $model->entry_date = $entry_date = date("Y-m-d");
                    $model->vehicle_id = $vehicle_id;
                    $model->current_unit_type = $current_unit_type;
                    $model->current_unit_id = $current_unit_id;
                    $model->unit_name_id = $unit_name_id;
                    $model->renewal_by = $renewal_by;  
                    $model->save();
                    $info_id = $model->id;
                    if ($info_id) {
                        $document_id = 18;
                        $maxSlNo = VehicleDocumentDetails::maxSlNo();
                        $detailsModel = new VehicleDocumentDetails;
                        $next_renewal_date = $this->getNextRenewalDate($duration, $issue_date);
                        if ($duration != '' && $issue_date != '' && $expense_with_vat >= 0) {
                            $detailsModel->vehicle_document_info_id = $info_id;
                            $detailsModel->vehicle_id = $vehicle_id;
                            $detailsModel->document_id = $document_id;
                            $detailsModel->entry_date = $entry_date;
                            $detailsModel->issue_date = $issue_date;
                            $detailsModel->duration = $duration;
                            $detailsModel->next_renewal_date = $next_renewal_date;
                            $detailsModel->expense = $expense_with_vat;
                            $detailsModel->is_active = 1;
                            $detailsModel->is_renewal = 0;
                            $detailsModel->max_sl_no = $maxSlNo;
                            $detailsModel->renewal_by = $renewal_by;
                            $detailsModel->other_amount = $other_amount;
                            $detailsModel->save();
    
                            $notificationLogData = VehicleNotificationLog::model()->findAll(array("condition" => "vehicle_id = '$vehicle_id' AND document_id = '$document_id' AND notification_type = 1 AND is_active = 1"));
                            foreach ($notificationLogData as $sData) {
                                VehicleNotificationLog::model()->updateByPk($sData->id, array('is_active' => 0));
                            }
                            $logModel = new VehicleNotificationLog;
                            $logModel->vehicle_id = $vehicle_id;
                            $logModel->document_id = $document_id;
                            $logModel->is_active = 1;
                            $logModel->vehicle_expire_date = $next_renewal_date;
                            $logModel->notification_type = 1; // 1 for document renewal type notifiation
                            $logModel->save();
                        }
                    }
                    if ($model->save(false)) {
                        echo "SAVING ---- $reg_no...... <br>";
                    } else {
                        echo "NOT SAVING --- $reg_no...... <br>";
                    }
                } else {
                    echo "NOT SAVING ---- $reg_no...... <br>";
                }
            }
            exit;
        } else {
            echo 'FILE NOT FOUND!';
        }
        // $this->redirect(array('upload'));
    }