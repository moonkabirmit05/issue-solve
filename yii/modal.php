<!-- widget create -->
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'soReportDialogBox',
    'options' => array(
        'title' => 'Sales Voucher Preview',
        'autoOpen' => false,
        'modal' => true,
        'width' => 1030,
        'resizable' => false,
    ),
));
?>
<div id='AjFlashReportSo' style="display:none;"></div>
<?php $this->endWidget(); ?>
<!-- js open call -->
<script>    
    $("#soReportDialogBox").dialog("open");
    $("#AjFlashReportSo").html(data.soReportInfo).show();
</script>
<!-- controller response -->
<?php
echo CJSON::encode(array(
    'status' => 'success',
    'newOrderNo' => $thisSellNoFormat . $thisNewSellNo,
    'lastSONO' => $model->so_no,
    'soReportInfo' => $this->renderPartial('soRport', array('data' => $data), true, true),
));
?>
<!-- view file -->

<style>
    table.poReportTab{
        float: left;
        border-collapse: collapse;
        font-family: serif;
        font-size: 12px;
    }
    table.poReportTab th{
        padding: 10px 0px;
        text-align: center;
        color: #000000;
    }
    table.poReportTab td{
        padding: 4px 2px;
        text-align: center;
    }
</style>
<?php
if ($data) {
    $yourCompanyInfo = YourCompany::model()->findByAttributes(array('is_active' => YourCompany::ACTIVE,));
    if ($yourCompanyInfo) {
        $yourCompanyName = $yourCompanyInfo->company_name;
        $yourCompanyVatRegNo = $yourCompanyInfo->vat_regi_no;
        $yourCompanyLocation = $yourCompanyInfo->location;
        $yourCompanyRoad = $yourCompanyInfo->road;
        $yourCompanyHouse = $yourCompanyInfo->house;
        $yourCompanyContact = $yourCompanyInfo->contact;
        $yourCompanyEmail = $yourCompanyInfo->email;
        $yourCompanyWeb = $yourCompanyInfo->web;
    } else {
        $yourCompanyName = '';
        $yourCompanyVatRegNo = '';
        $yourCompanyLocation = '';
        $yourCompanyRoad = '';
        $yourCompanyHouse = '';
        $yourCompanyContact = '';
        $yourCompanyEmail = '';
        $yourCompanyWeb = '';
    }
    ?>

    <?php
    echo "<div class='printBtn'>";
    $this->widget('ext.mPrint.mPrint', array(
        'title' => ' ', //the title of the document. Defaults to the HTML title
        'tooltip' => 'Print', //tooltip message of the print icon. Defaults to 'print'
        'text' => '', //text which will appear beside the print icon. Defaults to NULL
        'element' => '.printAllTableForThisReport', //the element to be printed.
        'exceptions' => array(//the element/s which will be ignored
            '.summary',
            '.search-form',
        ),
        'publishCss' => TRUE, //publish the CSS for the whole page?
        'visible' => !Yii::app()->user->isGuest, //should this be visible to the current user?
        'alt' => 'print', //text which will appear if image can't be loaded
        'debug' => FALSE, //enable the debugger to see what you will get
        'id' => 'print-div'         //id of the print link
    ));
    echo "</div>";
    ?>
    <script>
        var printCount = 1;
        $("#print-div").click(function () {
            if (printCount > 2) {
                exit;
            } else {
                if (printCount % 2 == 0) {
                    $("#invTitle").html("Marchent Copy");
                } else {
                    $("#invTitle").html("Customer Copy");
                }
                printCount++;
            }
        })
    </script>
    <div class='printAllTableForThisReport' style="float: left;">
        <table class="poReportTab" id="customerCopy">
            <tr>
                <td colspan="6"><span id="invTitle">Customer Copy</span></td>
            </tr>
            <tr>
                <td colspan="6" style="font-weight: bold;"><?php if (!empty($yourCompanyName))
        echo $yourCompanyName;
    ?></td>
            </tr>
            <tr>
                <td colspan="6"><?php if (!empty($yourCompanyHouse))
                    echo $yourCompanyHouse;
    ?><?php if (!empty($yourCompanyRoad))
                    echo ', ' . $yourCompanyRoad;
    ?><?php if (!empty($yourCompanyLocation))
                    echo ', ' . $yourCompanyLocation;
    ?>
                    <?php if (!empty($yourCompanyContact))
                        echo '<br><b> Phone: ' . $yourCompanyContact . "</b>";
                    ?>
    <?php if (!empty($yourCompanyEmail))
        echo '<br>' . $yourCompanyEmail;
    ?>
                    <?php if (!empty($yourCompanyWeb))
                        echo ' ' . $yourCompanyWeb;
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="6">
                        <?php
                        echo '<b>VAT REG.NO: </b>' . '- *********';
                        ?>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: left; font-weight: bold; border-top: 1px dotted #000000;">
                    <span style="float: left;">
                        <?php
                        $thisQuickSellNoFormat = VoucherNoFormate::model()->numberFormatOfThis(VoucherNoFormate::SELL_ORDER);
                        $customerInfo = Customers::model()->customerAllInfo(end($data)->customer_id);
                        if(isset(end($data)->memo_id)){
                            $memo_no = end($data)->memo_id;
                            echo $thisQuickSellNoFormat . end($data)->so_no;
                        }else{
                            $memo_no = "";
                            echo $thisQuickSellNoFormat . end($data)->so_no;
                        }

                        ?>
                    </span>
                    <span style="float: right;">Date: <?php echo end($data)->created_at; ?></span>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: left; border-bottom: 1px dotted #000000;">
                    Cashier: <?php 
                        // echo Users::model()->fullNameOfThis(end($data)->initiated_by); 
                        echo Users::model()->fullNameOfThis(end($data)->created_by); 
                    ?>
                </td>
            </tr>
            <tr>
                <td style="border-bottom: 1px dotted #000000;">SL</td>
                <td style="border-bottom: 1px dotted #000000;">Item Description</td>
                <td style="border-bottom: 1px dotted #000000;">MRP</td>
                <td style="border-bottom: 1px dotted #000000;">Qty</td>
                <td style="border-bottom: 1px dotted #000000;">Discount</td>
                <td style="text-align: right; border-bottom: 1px dotted #000000;">Total</td>
            </tr>
            <?php
            $i = 1;
            $grossTotalVat = 0;
            $grandTotal = 0;
            $grandTotalWithoutVat = 0;
            $serviceCharge = 0;
            $grandTotalWithSc = 0;
            ?>
                <?php foreach ($data as $d): ?>
                <tr class="<?php
                    if ($i % 2 == 0) {
                        echo 'even';
                    } else {
                        echo 'odd';
                    }
                    ?>">
                    <td><?php echo $i++; ?>.</td>
                    <?php
                    // $itemInfo = ProdModels::model()->findByPk($d->item_id);
                    $itemInfo = ProdModels::model()->findByPk($d->model_id);
                    $itemName = "<font color='red'>Removed!</font>";
                    $isVatableItem = 0;
                    if ($itemInfo) {
                        $itemName = $itemInfo->model_name;
                        $isVatableItem = $itemInfo->vatable;
                    }
                    ?>
                    <td style="text-align: left;"><?php echo $itemName; ?></td>
                    <td><?php echo number_format(($d->sell_price), 2); ?></td>
                    <td><?php echo number_format(($d->sell_qty), 2); ?></td>
                    <td><?php echo number_format(($d->discount), 2); ?></td>
                    <td style="text-align: right;">
                        <?php
                        $lineTotal = ($d->sell_qty * $d->sell_price) - $d->discount;
                        $lineTotalWithoutVat = ($d->sell_qty * $d->sell_price) - $d->discount;

                        echo number_format(($lineTotalWithoutVat), 2);
                        $grandTotal += $lineTotal;
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="5" style="text-align: right; border-top: 1px dotted #000000;">Sub Total: </td>
                <td style="text-align: right; border-top: 1px dotted #000000;"><?php echo number_format(($grandTotal), 2); ?></td>
            </tr>
            <?php if (end($data)->special_disc > 0 ) { ?>
                <tr>
                    <td colspan="5" style="text-align: right;">Special Discount:</td>
                    <td style="text-align: right;">
                        <?php echo number_format(round(end($data)->special_disc), 2);?>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="text-align: right; border-bottom: 1px dotted #000000; font-weight: bold;">Bill After Discount: </td>
                    <td style="text-align: right; border-bottom: 1px dotted #000000; font-weight: bold;"><?php echo number_format(round($grandTotal - end($data)->special_disc), 2); ?></td>
                </tr>
            <?php } ?>

            <?php if (end($data)->vat_per > 0 || end($data)->vat_amount > 0) { ?>

                <tr>
                    <td colspan="5" style="text-align: right;">vat (<?php echo end($data)->vat_per; ?> %): </td>
                    <td style="text-align: right;">
                        <?php echo number_format((end($data)->vat_amount), 2);?>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="text-align: right; border-bottom: 1px dotted #000000; font-weight: bold;">Bill After Vat: </td>
                    <td style="text-align: right; border-bottom: 1px dotted #000000; font-weight: bold;"><?php echo number_format(($grandTotal + end($data)->vat_amount - end($data)->special_disc), 2); ?></td>
                </tr>
            <?php } ?>
            <?php //if (end($data)->disc_precent > 0 || end($data)->disc_amount > 0) { ?>

                <!-- <tr>
                    <td colspan="5" style="text-align: right;">Discount (<?php echo end($data)->disc_precent; ?> %): </td>
                    <td style="text-align: right;">
                        <?php //echo number_format(round(end($data)->disc_amount), 2);?>
                    </td>
                </tr> -->
            <?php //} ?>
            
            <tr>
                <td colspan="5" style="text-align: right; border-bottom: 1px dotted #000000; font-weight: bold;">Net Payable: </td>
                <td style="text-align: right; border-bottom: 1px dotted #000000; font-weight: bold;">
                    <?php echo number_format(round(end($data)->net_amount), 2); ?>
                </td>
            </tr>
            
            <tr>
                <td colspan="5" style="text-align: right; border-bottom: 1px dotted #000000;">Payment: </td>
                <td style="border-bottom: 1px dotted #000000;"></td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: right;">CASH: </td>
                <td style="text-align: right;">
                    <?php echo number_format(round(end($data)->cash_amount), 2);?>
                </td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: right;">
                <?php 
                    if(end($data)->payment_mode == 2){
                        echo "Card";
                    }else if(end($data)->payment_mode == 3){
                        echo "Bkash";
                    }else if(end($data)->payment_mode == 4){
                        echo "Nagad";
                    }else{
                        echo "Card";
                    } 
                ?>: </td>
                <td style="text-align: right;">
                    <?php echo number_format(round(end($data)->other_amount), 2);?>
                </td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: right;">Paid Amount: </td>
                <td style="text-align: right;">
                    <?php 
                        $paidAmount = end($data)->cash_amount + end($data)->other_amount;
                        echo number_format(round($paidAmount), 2);
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="5" style="text-align: right;">Change Amount: </td>
                <td style="text-align: right;">
                    <?php
                    $changeAmount = $paidAmount - end($data)->net_amount;
                    if($changeAmount > 0){
                        echo number_format(round($changeAmount), 2);
                    }else{
                        echo 0.00;
                    }                    
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="padding-top: 15px;">All prices are including VAT.</td>
            </tr>
            <tr>
                <td colspan="6" style="border-top: 1px dotted #000000; padding-bottom: 15px;">Thanks for visiting <?php echo $yourCompanyName; ?></td>
            </tr>
        </table>
    <?php
} else {
    
}
?>
</div>

<script>
    $(function () {
        $(".ui-icon-closethick").click(function () {
            window.location.href = window.location.href;                                  
        });
    });
</script>