<?php

//model er search er condition
$condition = " t.is_all_received = 2 ";
$criteria->condition = $condition;

// auto selected value set
$store_id = $FactoryRequisition->store_id;
echo $store . $form->dropDownList(
    $model, 'store_id[]', Stores::storeList(Stores::TYPE_FACTORY_STORE), array(
    'prompt' => 'Select',
    'id' => 'storeId_' . $i,
    'options' => array(
        $store_id => array(
            'selected' => true
        )
    )
));

// for unique column from model
public function rules()
{
    return array(
        array('china_code', 'unique', 'caseSensitive' => FALSE, 'message' => 'Product Code has already been taken.'),
    );
}

// excel download process link
http://localhost/sharif_metal/supply_chain/distributor/DistributorTerrytoryEmployeeData
http://localhost/akash_dth/akash_dth/employees/employeeInfoReport

// Excel Data Save
public function actionInsertProduct() {
    $fileExist = file_exists(Yii::app()->basePath . '/../upload/products/prodlist.xls');
    if ($fileExist) {
        Yii::import('ext.phpexcel.PHPExcel', true);
        $objReader = new PHPExcel_Reader_Excel5;
        $objPHPExcel = $objReader->load('upload/products/prodlist.xls');
        $objWorksheet = $objPHPExcel->getActiveSheet();
        $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
        $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5
        $zero_val = 0;
        for ($row = 2; $row <= $highestRow; ++$row) {
            $cat = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
            $subCat = trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
            $modelName = trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
            $code = trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
            $hsCode = trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
            $sroName = trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());
            $units = trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue());

            // for date purpose
            if (trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue()) != '') {
                $date = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
                $dateTimeObject = PHPExcel_Shared_Date::ExcelToPHPObject($date);
                $frst_date = $dateTimeObject->format('Y-m-d');
                $frst_reg_dt = date("Y-m-d", strtotime($frst_date));
            }




            //echo $cat.'----'.$subCat.'--------'.$modelName.'------------'.$code.'---------'.$hsCode.'---------'.$sroName.'----------'.$units;
            //exit;
            $model = new ProdModels();
            $model->item_id = $cat;
            $model->brand_id = $subCat;
            $model->model_name = $modelName;
            $model->code = $code;
            $model->hs_code = $hsCode;
            $model->sro_name = $sroName;
            $model->unit_id = $units;
            if ($model->save(false)) {
                echo "SAVING $code...... <br>";
            } else {
                echo "NOT SAVING $code...... <br>";
            }
        }
        exit;
    } else {
        echo 'FILE NOT FOUND!';
    }
    $this->redirect(array('upload'));
}

// Base Url:
echo Yii::app()->baseUrl; 

//Custom DropDown Form Array
$activeStatus = array(1 => 'Yes', 2 => 'No');
echo $form->dropDownList($model, 'is_active', $activeStatus); 

//CHTML Button with Parameter
echo CHtml::button('Create', array('submit' => array('productionPlanning/createProductionPlan', 'so_no' => $modelBPO->so_no, 'po_no' => $modelBPO->order_no)));

// drop down list
echo $form->dropDownList(
		$model, 'supplier_id', CHtml::listData(Suppliers::model()->findAll(array('order' => 'company_name ASC')), 'id', 'company_name'), array(
	'prompt' => 'Select',
));

// Delete all by attribute
JournalEntry::model()->deleteAllByAttributes(['bill_no' =>$dt->bill_no]);  

// Auto Complete
?>
<td>
<input type="text" id="customer_name"/>
<?php echo $form->hiddenField($model, 'srvc_customer_id'); ?>
<script>
	$(function () {
		$("#customer_name").autocomplete({
			source: function (request, response) {
				var customerName = request.term;
				$.post('<?php echo Yii::app()->baseUrl ?>/index.php/srvcCustomer/jquery_customerSearch', {"customerName": customerName},
						function (data) {
							response(data);
						}, "json");
			},
			minLength: 2,
			select: function (event, ui) {
				$("#customer_name").val(ui.item.label);
				$("#StoreReq_srvc_customer_id").val(ui.item.value);
				return false;
			}
		});
	});
</script>
</td>

<?php
// Custom Query for all row
$sql = "SELECT * FROM rooms where current_status =1";
$connection = Yii::app()->db;
$command = $connection->createCommand($sql);
$data = $command->queryAll();

// criteria query
$criteria = new CDbCriteria();
$criteria->condition = "t.max_sl_no=$sl_no";        
$criteria->select = "d.model_name, t.*";        
$criteria->join = " INNER JOIN prod_models as d ON t.model_id=d.id ";
$criteria->order = " d.model_name asc";
$existingData = ComparativeStudy::model()->findAll($criteria);

// Custom Query for single row
$scSql = "SELECT SUM(total) as scTotal FROM `sales_details` WHERE `sales_id`=$sellItemId AND `parts_service` = 2";
$connection = Yii::app()->db;
$command = $connection->createCommand($scSql);
$scdata = $command->queryRow();

// Update By PK
RoomReservation::model()->updateByPk($reservationId, array('active_status' => 0));

//Update All
OtherReqDR::model()->updateAll(array( 'prod_id'=>"$dt->prod_id", 'job_card_no'=>"$jobCardNo" ), "req_id=$dt->id");

// Criteria use
$criteria = new CDbCriteria();
$criteria->select = "id";
$criteria->condition = "cs_bill_id=$id";
$allData = CsBillParts::model()->findAll($criteria);

// beforeSave
public function beforeSave() {
    if ($this->isNewRecord) {
        $this->created_datetime = new CDbExpression('NOW()');
        $this->created_by = Yii::app()->user->getId();
    } else {
        $this->updated_datetime = new CDbExpression('NOW()');
        $this->updated_by = Yii::app()->user->getId();
    }
    return parent::beforeSave();
}

// FinByPK Data Process
$order = AR_Orders::model()->findByPk($id); 

// Single Data Process (Without Primary Key)
$prodQualityInfo=  ProdItems::model()->findByAttributes(array('item_name'=>$prodQuality));

// With Condition Than
$wpParts = WpParts::model()->findByAttributes(array('car_id' => $carId, 'parts_id' => $ep_prod_id, 'is_active' => 1), array(
    'condition' => 'qty >= :quantity',
    'params' => array('quantity' => $qty
))
);

// For update
$sql = "UPDATE employees SET is_active = 2 WHERE branch_id != 4 and is_active != 2";
$connection = Yii::app()->db;
$command = $connection->createCommand($sql);
$data = $command->execute();

// get login User Id
$loginUserId = Yii::app()->user->getId();

// model relation style
public function relations() {
    return array(
        'prodBrands' => array(self::HAS_MANY, 'ProdBrands', 'item_id'),
        'prodModels' => array(self::HAS_MANY, 'ProdModels', 'item_id'),
    );
}

// disable column for admin page
array(
    'value' => '$data->approve_status',
    'class' => 'CCheckBoxColumn',
    'disabled' => '$data->approve_status == 0',
)
// sort and pagination from model search function
return new CActiveDataProvider($this, array(
    'criteria'=>$criteria,
    'sort' => array(
        'defaultOrder' => 't.id desc',
    ),
    'pagination' => array(
        'pageSize' => 20,
    ),

));


// dropdown list with selected value
$reportType = [
    1 => 'All',
    2 => 'Increment',
    3 => 'Promotion',                  
];
$model->report_type = 1; //if default data need then assign value.
echo $form->dropDownList($model, 'report_type', $reportType, array('prompt' => 'Select', 'style' => 'width:73%;')); 
                
// month picker
$this->widget('ext.EJuiMonthPicker.EJuiMonthPicker', array(
    'model' => $model,
    'attribute' => 'startDate',
    'options' => array(
        'yearRange' => '-5:+15',
        'dateFormat' => 'yy-mm',
    ),
    'htmlOptions' => array(
        'autocomplete' => 'off',
    ),
));
echo $form->error($model, 'month');

// model validation 
if ($model->validate()) {
    echo "Ok";
} else {
    $errores = $model->getErrors();
    print_r($errores);
}


// max val of this model
$max_sl_no = AssetInfo::model()->maxValOfThis('AssetInfo', 'max_sl_no', 'maxSlNo');
public function maxValOfThis($whatModel, $whatField, $whatFieldsMaxVal) {

    $maxVal = 0;
    $maxValL = 0;
    $criteria = new CDbCriteria();
    $criteria->select = 'MAX(' . $whatField . ') as ' . $whatFieldsMaxVal . '';
    $maxValInfo = $whatModel::model()->findAll($criteria);
    if ($maxValInfo) {
        foreach ($maxValInfo as $mvi):
            $maxVal = $mvi->$whatFieldsMaxVal;
        endforeach;
    }

    $maxNumberVal = floatval($maxVal + 1);

    if ($whatModel == "ProdModels") {
        if ($maxNumberVal == 1) {
            $list = Yii::app()->db->createCommand("SELECT `AUTO_INCREMENT`
            FROM  INFORMATION_SCHEMA.TABLES
            WHERE TABLE_SCHEMA = '" . Yii::app()->params->dbName . "'
            AND   TABLE_NAME   = 'prod_models'")->queryAll();

            foreach ($list as $item) {
                $rs = $item['AUTO_INCREMENT'];
            }
            $maxNumberVal = $rs;
        }
    }


    return $maxNumberVal;
}

// autocomplete search
public function actionjquery_showPdcReceiveNo()
{       
    $search_PDC_Receive_No = trim($_POST['search_PDC_Receive_No']);
    $criteria = new CDbCriteria();
    $criteria->addColumnCondition(['is_approved' => 1]);
    if ($search_PDC_Receive_No) {
        $criteria->compare('id',$search_PDC_Receive_No,true);
    }       
    $criteria->order = 'id DESC';
    $criteria->limit = 20;
    $PdcReceive = PdcReceive::model()->findAll($criteria);
    if ($PdcReceive) {
        foreach ($PdcReceive as $pdc) {
            $value = $label = $pdc->id;
            $results[] = array(
                'value' => $value,
                'label' => $label,
            );
        }
    } else {
        $results[] = array('value' => "0", 'label' => "No PDC Receive No. Found");
    }
    echo json_encode($results);
}