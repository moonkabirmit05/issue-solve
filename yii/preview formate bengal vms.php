<table align="center" width="100%" class="listtable" border="0" cellspacing="0" cellpadding="0">
    <?php
        $companyLogo = $companyTitle = $companyHouse = $companyRoad = $companyLocation = $companyContact = $companyEmail = $companyWeb = "";
        if($model->user_company != ""){
            $unitTypeId = $model->user_company;
        }else{
            $unitTypeId = 95;
        }
        $companyInfo = UnitType::model()->findByPk($unitTypeId);
        if($companyInfo){
            $companyLogo = $companyInfo->logo_name != "" ? $companyInfo->logo_name : $companyLogo;
            $companyTitle = $companyInfo->title != "" ? $companyInfo->title : $companyTitle;
            $companyHouse = $companyInfo->house != "" ? $companyInfo->house : $companyHouse;
            $companyRoad = $companyInfo->road != "" ? $companyInfo->road : $companyRoad;
            $companyLocation = $companyInfo->location != "" ? $companyInfo->location : $companyLocation;
            $companyContact = $companyInfo->contact != "" ? $companyInfo->contact : $companyContact;
            $companyEmail = $companyInfo->email != "" ? $companyInfo->email : $companyEmail;
            $companyWeb = $companyInfo->web != "" ? $companyInfo->web : $companyWeb;
        }
    ?>
    <tr>
        <td style="text-align:center;">
            <img style="width: 80px;" src="<?php echo Yii::app()->baseUrl; ?>/upload/companyinfo/<?php echo $companyLogo; ?>" />
        </td>
    </tr>
    <tr>
        <td style="text-align:center; font-size: 16px"> <b><?php echo $companyTitle;?></b></td>
    </tr>
    <tr>
        <td style="text-align:center; font-size: 16px">
            <?php
                echo $companyHouse != "" ? "House:".$companyHouse."," : "";                                          
                echo $companyRoad != "" ? "Road:".$companyRoad."," : "";                                          
                echo $companyLocation != "" ? $companyLocation : "";                                          
            ?>
        </td>
    </tr>
    <tr>
        <td style="text-align:center; font-size: 16px">
            <?php
                echo $companyContact != "" ? "Tel:".$companyContact."," : "";                                          
                echo $companyEmail != "" ? "Email:".$companyEmail."," : "";                                          
                echo $companyWeb != "" ? "Web:".$companyWeb : "";                                         
            ?>
        </td>
    </tr>
</table>