<?php
// Remove Special Character
$res = str_replace( array( '\'', '"',',' , ';', '<', '>' ), ' ', $str);

//current time
date_default_timezone_set("Asia/Dhaka");

// Amount in word
$amountInWord = new AmountInWord();
echo $amountInWord->convert(intval($totalAmountWithDiscount)) . ' Taka Only.';
