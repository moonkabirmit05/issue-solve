truncate table employees;

select full_name, emp_id_no, r.name, a.area_name, t.territory_name, d.department_name, g.title  from employees
left join region as r
ON employees.region_id = r.id
left join area as a
ON employees.area_id = a.id
left join territory as t
ON employees.territory_id = t.id
left join departments as d
ON employees.department_id = d.id
left join grade as g
ON employees.grade_id = g.id

//Search sepecific COLUMNS name in full DB
select * from INFORMATION_SCHEMA.COLUMNS where COLUMN_NAME = 'sales_office_id' AND TABLE_SCHEMA = "globe_biscuit" order by TABLE_NAME


